¡Bienvenido!

Gracias por tomarte el tiempo de leer y analizar nuestro proyecto, presentamos algunos comentarios para que puedas comprender correctamente nuestro proceso:

En la carpeta "Etapa1" vas a encontrarte con 4 documentos fundamentales para el desarrollo de este trabajo. 

Por un lado; tenemos "" en donde se encuentra el codigo necesario para la conexión mediante WiFi, el uso del ultrasonido, la relación con la base de datos y el funcionamiento de la lámpara 220V. 
Por motivos ajenos al equipo de trabajo, todo el desarrollo se realizó a través de la plataforma de Arduino. 
Se utilizaron las siguientes librerias		-""
						-""
						-""
Por otro, en "index.html" y "style.css" creamos una página web -hosteada en gitlab- para poder visualizar la lectura de la distancia, tanto al utilizar el método set como el método push. Asi mismo, agregamos dos botones para el encendido y apagado de la lámpara de forma automática -teniendo en cuenta la distancia medida- o de forma manual.

Finalmente; en el archivo "login.js" realizamos el codigo requerido para la conexión entre el microcontrolador y la base de datos, asi mismo como las diversas lecturas de datos y las respuestas de los botones mencionados anteriomente.

En la carpeta "Etapa2" te encontrarás con una copia de los mismos documentos, pero con la adición de un nuevo modo de operación. Se modifica el archivo "" para que el microcontrolador funcione tanto como estación como punto de acceso, habilitando al usuario final elegir la red WiFi a la que se va a conectar.