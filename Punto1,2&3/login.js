var usuario = document.getElementById("mailId");
var contra = document.getElementById("passId");
var botonESTADO = document.getElementById("botonest");
var botonMETODO = document.getElementById ("botonmet")
var set = document.getElementById("distanciaxd");
var push1 = document.getElementById("distpush1");
var push2 = document.getElementById("distpush2");
var push3 = document.getElementById("distpush3");
var push4 = document.getElementById("distpush4");
var push5 = document.getElementById("distpush5");

var estado = false;
var metodo = false;
var nombremet = 0;
var variable = 0;
var contador = 0;
var datapush = "";

/*CONFIGURACIÓN FIREBASE*/
var firebaseConfig = {
  apiKey: "AIzaSyCKZHhsHhPmwQ5V6Sw7n7KyXfGe03l3oVU",
  authDomain: "ea-1-546e1.firebaseapp.com",
  databaseURL: "https://ea-1-546e1-default-rtdb.firebaseio.com",
  projectId: "ea-1-546e1",
  storageBucket: "ea-1-546e1.appspot.com",
  messagingSenderId: "182144898748",
  appId: "1:182144898748:web:a75c79406d23d4d971e2b9"
};

firebase.initializeApp(firebaseConfig);

/*LECTURA DE VALORES DESDE SET*/ 
firebase.database().ref('/distancia').on('value', (snapshot) => {
  const data = snapshot.val();
  set.innerHTML = data;
  console.log(data);
});

/*LECTURA DE RUTAS CON PUSH */
firebase.database().ref('/rutapush').get('value', (snapshot) => {
  const pathpush = snapshot.val();
  variable = pathpush;
  contador = contador + 1;
  if (contador == 5){
    contador = 0;
    push1.innerHTML = "";
    push2.innerHTML = "";
    push3.innerHTML = "";
    push4.innerHTML = "";
    push5.innerHTML = "";
  }
})

/*LECTURA DE VALORES CON PUSH */
firebase.database().ref('/distanciapush/' + variable).on('value', (snapshot) => {
  datapush = snapshot.val();
})

if (contador == 0)
{
  push1.innerHTML = datapush;
  console.log("valor en push1" + datapush);
}
else if (contador == 1)
{
  push2.innerHTML = datapush;
  console.log("valor en push2" + datapush);
}
else if (contador == 2)
{
  push3.innerHTML = datapush;
  console.log("valor en push3" + datapush);
}
else if (contador == 3)
{
  push4.innerHTML = datapush;
  console.log("valor en push4" + datapush);
}
else if (contador == 4)
{
  push5.innerHTML = datapush;
  console.log("valor en push5" + datapush);
}


/*BOTON METODO AUTOMÁTICO/MANUAL*/ 
botonMETODO.addEventListener ("click", cambiarmet, false);
function cambiarmet(){
    metodo =! metodo;
    console.log (metodo);
    firebase.database().ref ('/manual').set(metodo);
}

firebase.database().ref('/manual').on('value', (snapshot) => {
    const datomet = snapshot.val();
    nombremet = datomet
    console.log ("estado de metodo: " + datomet);
})

botonMETODO.addEventListener ("click", nombre, false);
function nombre(){
    if (nombremet == false)
    {
      botonMETODO.innerHTML = "AUTOMÁTICO";
    }
    else 
    {
      botonMETODO.innerHTML = "MANUAL";
    }
    
}

/*DENTRO DE MANUAL, CAMBIAR ESTADO LED*/
botonESTADO.addEventListener ("click", settear, false);
function settear (){
    estado =! estado;
    console.log(estado);
    firebase.database().ref('/led').set(estado);
}
